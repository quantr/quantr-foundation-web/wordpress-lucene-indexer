package hk.quantr.wordpress.indexer;

import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.PropertyUtil;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class WordpressLuceneIndexer {

    public static final Logger logger = Logger.getLogger(WordpressLuceneIndexer.class.getName());
    final File baseDir = new File("repo");

    static {
        InputStream stream = WordpressLuceneIndexer.class.getClassLoader().getResourceAsStream("logging.properties");
        try {
            LogManager.getLogManager().readConfiguration(stream);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Options options = new Options();
        options.addOption("v", "version", false, "display version");
        options.addOption(Option.builder("a")
                .required(true)
                .hasArg()
                .argName("action")
                .desc("index/search")
                .longOpt("action")
                .build());
        options.addOption("c", "create", true, "create new index");
        options.addOption("i", "index", true, "index folder path, for search only");
        options.addOption("o", "output", true, "output format, for search only");
        options.addOption(Option.builder("u")
                .required(false)
                .hasArg()
                .argName("url")
                .desc("wordpress restful url prefix, e.g. https://www.quantr.foundation/wp-json")
                .longOpt("url")
                .build());
        options.addOption("q", "token", true, "search query");

        if (Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar gitlab-lucene-indexer-xx.jar [OPTION] <input file>", options);
            return;
        }
        if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) {
            System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));

            TimeZone utc = TimeZone.getTimeZone("UTC");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            format.setTimeZone(utc);
            Calendar cl = Calendar.getInstance();
            try {
                Date convertedDate = format.parse(PropertyUtil.getProperty("main.properties", "build.date"));
                cl.setTime(convertedDate);
                cl.add(Calendar.HOUR, 8);
            } catch (java.text.ParseException ex) {
                logger.log(Level.INFO, ex.getMessage(), ex);
            }
            System.out.println("build date : " + format.format(cl.getTime()) + " HKT");
            return;
        }

        new WordpressLuceneIndexer().start(options, args);
    }

    private void start(Options options, String[] args) {
        try {
            CommandLineParser cliParser = new DefaultParser();
            CommandLine cmd = cliParser.parse(options, args);

            if (cmd.getOptionValue("action").equals("index")) {
                FileUtils.deleteDirectory(baseDir);
                if (cmd.getOptionValue("c") != null && cmd.getOptionValue("c").toLowerCase().equals("y")) {
                    FileUtils.deleteDirectory(new File("index"));
                }
                String urlPrefix = cmd.getOptionValue("u");
                if (urlPrefix == null) {
                    System.err.println("Please provide -u");
                    System.exit(1);
                }
                String json = get(urlPrefix + "/wp/v2/posts");
                System.out.println(CommonLib.prettyFormatJson(json));
                JSONArray arr = new JSONArray(json);
                for (int x = 0; x < arr.length(); x++) {
                    JSONObject obj = arr.getJSONObject(x);
                    int id = obj.getInt("id");
                    String date = obj.getString("date");
                    String title = obj.getJSONObject("title").getString("rendered");
                    String content = obj.getJSONObject("content").getString("rendered");
                    String excerpt = obj.getJSONObject("excerpt").getString("rendered");
                    String link = obj.getString("link");

                    JSONObject user = new JSONObject(get(obj.getJSONObject("_links").getJSONArray("author").getJSONObject(0).getString("href")));
                    String author_name = user.getString("name");

                    String source_url = null;
                    if (!obj.getJSONObject("_links").isNull("wp:featuredmedia")) {
                        JSONObject featured_media = new JSONObject(get(obj.getJSONObject("_links").getJSONArray("wp:featuredmedia").getJSONObject(0).getString("href")));
                        source_url = featured_media.getJSONObject("media_details").getJSONObject("sizes").getJSONObject("full").getString("source_url");
                        System.out.println("source_url=" + source_url);
                    }

                    System.out.println(id + " : " + author_name + " : " + date + " : " + title + " : " + link);

                    // index
                    Analyzer analyzer = new StandardAnalyzer();
                    IndexWriterConfig indexConfig = new IndexWriterConfig(analyzer);
                    File path = new File("index");
                    Directory directory = FSDirectory.open(path.toPath());
                    IndexWriter indexWriter = new IndexWriter(directory, indexConfig);
                    Document document = new Document();
                    document.add(new StringField("category", "wordpress", Field.Store.YES));
                    document.add(new StringField("sub-category", "post", Field.Store.YES));
                    document.add(new StringField("id", String.valueOf(id), Field.Store.YES));
                    document.add(new StringField("date", date, Field.Store.YES));
                    document.add(new TextField("title", title, Field.Store.YES));
                    document.add(new TextField("content", content, Field.Store.YES));
                    document.add(new TextField("excerpt", excerpt, Field.Store.YES));
                    document.add(new StringField("link", link, Field.Store.YES));
                    document.add(new StringField("author_name", author_name, Field.Store.YES));
                    if (source_url != null) {
                        document.add(new StringField("featured_image", source_url, Field.Store.YES));
                    }
                    indexWriter.addDocument(document);
                    indexWriter.close();
                }
            } else if (cmd.getOptionValue("action").equals("search")) {
                String q = cmd.getOptionValue("q");
                if (q == null) {
                    System.out.println("please provide query by -q");
                    return;
                }

                String myQuery = q;
                if (!q.startsWith("content:") && !q.contains(":")) {
                    myQuery = "content:" + myQuery;
                }

                try {
                    Analyzer analyzer = new StandardAnalyzer();

                    QueryParser queryParser = new QueryParser("MyParser", analyzer);
//					Query query = queryParser.parse("content:peter@quantr.hk");
                    Query query = queryParser.parse(myQuery);

                    File indexFolder;
                    if (cmd.hasOption("i")) {
                        indexFolder = new File(cmd.getOptionValue("i"));
                    } else {
                        indexFolder = new File("index");
                    }
                    Directory directory = FSDirectory.open(indexFolder.toPath());
                    IndexReader indexReader = DirectoryReader.open(directory);
                    IndexSearcher searcher = new IndexSearcher(indexReader);
                    TopDocs topDocs = searcher.search(query, 10);
                    if (!cmd.hasOption("o") || !cmd.getOptionValue("o").equals("json")) {
                        System.out.println("Search result: " + topDocs.totalHits);
                        System.out.println("-".repeat(100));
                    }
                    ScoreDoc[] scoreDocs = topDocs.scoreDocs;
                    JSONArray arr = new JSONArray();
                    for (ScoreDoc scoreDoc : scoreDocs) {
                        int docId = scoreDoc.doc;
                        float score = scoreDoc.score;
                        Document doc = searcher.doc(docId);

                        if (cmd.hasOption("o") && cmd.getOptionValue("o").equals("json")) {
                            JSONObject obj = new JSONObject();
                            obj.put("category", doc.get("category"));
                            obj.put("sub-category", doc.get("sub-category"));
                            obj.put("docID", docId);
                            obj.put("score", score);
                            obj.put("id", doc.get("id"));
                            obj.put("date", doc.get("date"));
                            obj.put("title", doc.get("title"));
                            obj.put("content", doc.get("content"));
                            obj.put("excerpt", doc.get("excerpt"));
                            obj.put("link", doc.get("link"));
                            obj.put("author_name", doc.get("author_name"));
                            arr.put(obj);
                        } else {
                            System.out.println("docID= " + docId + " , score= " + score);
                            System.out.println("category: " + doc.get("category"));
                            System.out.println("sub-category: " + doc.get("sub-category"));
                            System.out.println("id: " + doc.get("id"));
                            System.out.println("date: " + doc.get("date"));
                            System.out.println("title: " + doc.get("title"));
//							System.out.println("content: " + doc.get("content"));
                            System.out.println("link: " + doc.get("link"));
                            System.out.println("author_name: " + doc.get("author_name"));
                            System.out.println("excerpt: " + doc.get("excerpt"));
                            System.out.println("-".repeat(100));
                        }
                    }

                    if (cmd.hasOption("o") && cmd.getOptionValue("o").equals("json")) {
                        System.out.println(CommonLib.prettyFormatJson(arr.toString()));
                    }
                    indexReader.close();
                } catch (IOException | org.apache.lucene.queryparser.classic.ParseException ex) {
                    ex.printStackTrace();
                }
            } else {
                System.out.println("wrong action");
            }
        } catch (IOException | ParseException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    public String get(String url) throws IOException {
        String str = null;
        do {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(url);
            CloseableHttpResponse response = httpclient.execute(httpGet);
            try {
                str = IOUtils.toString(response.getEntity().getContent(), "utf8");
            } catch (Exception ex2) {
                logger.log(Level.INFO, "Exception, sleep : " + url);
                logger.log(Level.SEVERE, null, ex2);
                str = null;
            }
            httpclient.close();
        } while (str == null || str.contains("Too Many Requests"));
        return str;
    }
}
